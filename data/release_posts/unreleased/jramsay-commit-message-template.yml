---
feature:
  secondary:
    - name: "Configure default commit message for applied Suggestions"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: true
      documentation_link: https://docs.gitlab.com/ee/user/discussions/#configure-the-commit-message-for-applied-suggestions
      image_url: "/images/unreleased/suggestion-template.png"
      reporter: jramsay
      stage: create
      categories:
        - "Code Review"
      issue_url: https://gitlab.com/gitlab-org/gitlab/issues/13086
      description: |
        [Suggesting changes](https://docs.gitlab.com/ee/user/discussions/#suggest-changes)
        in merge requests makes proposing improvements easy, but if you also
        use a push rule to validate the format of commit messages is a valid
        [Conventional Commit](https://www.conventionalcommits.org/en/v1.0.0/),
        it wasn't possible to apply the suggested change. This is because
        the default commit message created by GitLab didn't match the validation pattern
        for the push rule.

        GitLab 12.7 now supports configuring a commit message template for the
        commits created by GitLab when applying a suggested change, and are
        valid according to your commit message push rule.

        Thanks [Fabio Huser](https://gitlab.com/fh1ch) and Siemens!
